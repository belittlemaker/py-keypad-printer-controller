from abc import ABCMeta, abstractmethod
from pyoctoprintapifacade import OctoPrintApi
from lib import SerialPort

from threading import Thread


class AbstractController(Thread, metaclass=ABCMeta):

    _PRINTER_STATE_ONLINE = True
    _PRINTER_STATE_OFFLINE = False

    def __init__(self, serial: SerialPort, printer_api: OctoPrintApi):
        Thread.__init__(self)
        self.daemon = True

        self.__printer_api = printer_api
        self.__printer_state = self._PRINTER_STATE_OFFLINE
        self.__serial = serial

    def run(self):
        while True:
            self._check(self._is_operational())

    @abstractmethod
    def _check(self, is_operational: bool) -> None:
        pass

    def _has_state_changed(self, printer_state_new: bool):
        changed = printer_state_new != self.__printer_state
        self.__printer_state = printer_state_new

        return changed

    def _is_operational(self):
        return self.__printer_api.get_connection_facade().is_operational()

    def _read_serial(self) -> str:
        return self.__serial.read()

    def _write_serial(self, output: str) -> None:
        self.__serial.write(output)
