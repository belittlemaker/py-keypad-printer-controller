from action import BuilderAction
from config import GeneralConfig
from controller import AbstractController
from lib import SerialPort
from parser import Parser
from pyoctoprintapifacade import OctoPrintApi

import logging


class ActionController(AbstractController):

    def __init__(self, serial: SerialPort, printer_api: OctoPrintApi):
        super().__init__(serial, printer_api)

        self._actions_by_state = {
            self._PRINTER_STATE_ONLINE:  BuilderAction.get_actions_online(printer_api),
            self._PRINTER_STATE_OFFLINE: BuilderAction.get_actions_offline(printer_api)
        }
        self._parser = Parser()

    def _check(self, is_operational: bool) -> None:
        if self._has_state_changed(is_operational):
            self.__reset_actions_on_change(is_operational)

        self._parser.parse(self._read_serial())
        if self._parser.get_label():
            actions: list = self._actions_by_state.get(is_operational)
            self._check_actions(actions)

    def _check_actions(self, actions: list) -> None:
        for action in actions:
            if action.serial_match(self._parser.get_label()):
                logging.info('pushed - ' + str(self._parser.get_label()))
                try:
                    action.run(self._parser.get_data())
                except Exception as e:
                    self._write_serial(GeneralConfig.printer_exception_output)
                    logging.error(str(e))
                action.sleep()
            else:
                action.reset()

    def __reset_actions_on_change(self, printer_state: bool):
        actions = self._actions_by_state.get(printer_state)
        for action in actions:
            action.reset()
