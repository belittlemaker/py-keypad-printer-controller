from config import GeneralConfig
from controller import AbstractController
from lib import SerialPort
from pyoctoprintapifacade import OctoPrintApi

from time import sleep


class StateController(AbstractController):

    def __init__(self, serial: SerialPort, printer_api: OctoPrintApi):
        super().__init__(serial, printer_api)

        self.__output_by_printer_state = {
            self._PRINTER_STATE_OFFLINE: self.__is_printer_offline,
            self._PRINTER_STATE_ONLINE: self.__is_printer_online,
        }
        self.__is_printer_offline()

    def _check(self, is_operational: bool) -> None:
        self.__output_by_printer_state.get(is_operational)()
        sleep(GeneralConfig.state_sleep)

    def __is_printer_offline(self) -> None:
        self._write_serial(GeneralConfig.printer_offline_output)

    def __is_printer_online(self) -> None:
        self._write_serial(GeneralConfig.printer_online_output)
