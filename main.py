from config import GeneralConfig
from config import LoggingConfig
from config import OctoprintConfig
from controller import ActionController
from controller import StateController
from pyoctoprintapifacade import OctoPrintApi
from lib import SerialPort

import logging
from time import sleep

try:
    logging.basicConfig(filename=LoggingConfig.path,
                        filemode=LoggingConfig.mode,
                        level=LoggingConfig.level,
                        format=LoggingConfig.formatter,
                        datefmt=LoggingConfig.date_format)

    printer_api = OctoPrintApi(OctoprintConfig.api_key, OctoprintConfig.domain)
    if OctoprintConfig.logging_traces:
        printer_api.enable_logging_traces()

    serial = SerialPort(GeneralConfig.serial_path, GeneralConfig.serial_baudrate, GeneralConfig.encoding)
    serial.connect()

    action_controller = ActionController(serial, printer_api)
    state_controller = StateController(serial, printer_api)

    action_controller.start()
    state_controller.start()

    while True:
        sleep(0.1)

except KeyboardInterrupt:
    logging.info("CTRL-C pressed...")
except ValueError as value_error:
    logging.error(value_error)
finally:
    serial.close()
