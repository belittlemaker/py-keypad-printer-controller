# own connection data to Octoprint API
# https://docs.octoprint.org/en/master/api/general.html

api_key = ''
domain = '127.0.0.1/api'
port = '/dev/ttyUSB1'
protocol = 'http'

logging_traces = False
