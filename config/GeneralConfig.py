serial_path = "/dev/ttyUSB0"
serial_baudrate = 9600

action_sleep = 3
state_sleep = 5

printer_offline_output = "1"
printer_online_output = "2"
printer_exception_output = "3"

encoding = 'utf8'
