from datetime import datetime
import logging

# DOC: https://docs.python.org/3/library/logging.html

path = './log/log_' + datetime.now().strftime('%Y%m%d') + '.log'
file_extension = '.log'
level = logging.INFO
formatter = '%(asctime)s - %(levelname)s - %(message)s'
date_format = '%Y-%m-%d %H:%M:%S'
mode = 'a'
