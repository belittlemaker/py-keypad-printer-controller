# py-keypad-printer-controller

Bridge between Octoprint API and action keyboard by Arduino

![](assets/img/keypad.png)

Hardware
--------
- Raspberry Pi
  - https://www.raspberrypi.org/products/
    - Recommended version 3 or higher
    - Used for this project: Raspberry Pi 3b
  - OS: OctoPi
    - https://octoprint.org/download/
- Arduino Nano
  - https://store.arduino.cc/arduino-nano
  - https://gitlab.com/belittlemaker/ino_keypad_printer_controller
- Keypad:
  - https://www.thingiverse.com/thing:4846136

Steps before execution
----------------------
1. Review the configuration files:
   - config/GeneralConfig.py
   - config/LoggingConfig.py
   - config/OctoprintConfig.py
2. Change your own data
    

External Libraries
------------------
- Install package pyoctoprintapifacade in to the folder:
```
>> py -m pip install git+https://gitlab.com/belittlemaker/py-octoprint-api-facade.git@master
```
- Review requirements file

Startup program
---------------
- Add bash file "py_keypad_printer_controller.sh" to crontab, Example:
```
@rebbot /home/pi/py_keypad_printer_controller/bin/py_keypad_printer_controller.sh
```
- Uncomment process delay lines if needed
```
#sleep 30
#pid=$!
#wait $pid
```
Button Actions
--------------
Case A: If the printer isn't connected to Octoprint
- Row 1:
  1. Connect

Case B: If the printer is connected to Octoprint
- Row 1:
  1. Disconnect
  2. Go Home Bed 
  3. Pause/resume
     - First tap: Pause
     - Second tap: Resume
  4. Cancel
- Row 2.
  1. Level Bed
  2. Hotend: 200ºC
     - Next taps: +5ºC
  3. Bed: 50ºC
     - Next taps: +5ºC
  4. Hotend & Bed: 0ºC
