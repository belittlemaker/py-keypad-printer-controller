from action.FactoryAction import FactoryAction
from pyoctoprintapifacade import OctoPrintApi


class BuilderAction:

    @staticmethod
    def get_actions_online(printer_api: OctoPrintApi) -> list:
        return [
            FactoryAction.get_cancel_action(printer_api),
            FactoryAction.get_cold_action(printer_api),
            FactoryAction.get_disconnect_action(printer_api),
            FactoryAction.get_home_action(printer_api),
            FactoryAction.get_hot_bed_action(printer_api),
            FactoryAction.get_hot_tool_action(printer_api),
            FactoryAction.get_level_action(printer_api),
            FactoryAction.get_pause_action(printer_api)
        ]

    @staticmethod
    def get_actions_offline(printer_api: OctoPrintApi) -> list:
        return [
            FactoryAction.get_connect_action(printer_api)
        ]
