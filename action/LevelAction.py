from action.AbstractStatesAction import AbstractStatesAction
from pyoctoprintapifacade import OctoPrintApi


class LevelAction(AbstractStatesAction):

    __JOG_SPEED = 1500
    __STATE_TO_BED = 'to_bed'
    __STATE_BOTTOM_RIGHT = 'button_right'
    __STATE_TOP_RIGHT = 'top_right'
    __STATE_TOP_LEFT = 'top_left'
    __STATE_BOTTOM_LEFT = 'button_left'

    def __init__(self, printer_api: OctoPrintApi):
        super().__init__(printer_api)

        self.__bed_points_by_state = {
            self.__STATE_TO_BED:       {'x': 40,   'y': 60},    # bottom - left
            self.__STATE_BOTTOM_RIGHT: {'x': 140,  'y': 0},     # bottom - right
            self.__STATE_TOP_RIGHT:    {'x': 0,    'y': 140},   # up - right
            self.__STATE_TOP_LEFT:     {'x': -140, 'y': 0},     # up - left
            self.__STATE_BOTTOM_LEFT:  {'x': 0,    'y': -140},  # bottom - left
        }

    def _run_states(self) -> dict:
        return {
            self.__STATE_TO_BED: self.run_to_bed,
            self.__STATE_BOTTOM_RIGHT: self.run_button_right,
            self.__STATE_TOP_RIGHT: self.run_top_right,
            self.__STATE_TOP_LEFT: self.run_top_left,
            self.__STATE_BOTTOM_LEFT: self.run_button_left,
        }

    def run_to_bed(self, data: str = None) -> None:
        self._printer.get_printer_facade().go_home()
        self.__printer_jog(self.__STATE_TO_BED)
        self._set_state(self.__STATE_BOTTOM_RIGHT)

    def run_button_right(self, data: str = None) -> None:
        self.__printer_jog(self.__STATE_BOTTOM_RIGHT)
        self._set_state(self.__STATE_TOP_RIGHT)

    def run_top_right(self, data: str = None) -> None:
        self.__printer_jog(self.__STATE_TOP_RIGHT)
        self._set_state(self.__STATE_TOP_LEFT)

    def run_top_left(self, data: str = None) -> None:
        self.__printer_jog(self.__STATE_TOP_LEFT)
        self._set_state(self.__STATE_BOTTOM_LEFT)

    def run_button_left(self, data: str = None) -> None:
        self.__printer_jog(self.__STATE_BOTTOM_LEFT)
        self._set_state(self.__STATE_BOTTOM_RIGHT)

    def _get_serial(self) -> str:
        return "level"

    def _get_state_begin(self) -> str:
        return self.__STATE_TO_BED

    def __printer_jog(self, state: str) -> None:
        point: dict = self.__bed_points_by_state.get(state)
        self._printer.get_printer_facade().jog(x=point.get('x'), y=point.get('y'), speed=self.__JOG_SPEED)
