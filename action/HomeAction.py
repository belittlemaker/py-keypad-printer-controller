from action.AbstractAction import AbstractAction


class HomeAction(AbstractAction):

    def run(self, data: str = None) -> None:
        self._printer.get_printer_facade().go_home()

    def _get_serial(self) -> str:
        return "home"
