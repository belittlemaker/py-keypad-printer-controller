from action.AbstractAction import AbstractAction
from action.CancelAction import CancelAction
from action.ColdAction import ColdAction
from action.ConnectAction import ConnectAction
from action.DisconnectAction import DisconnectAction
from action.HomeAction import HomeAction
from action.HotBedAction import HotBedAction
from action.HotToolAction import HotToolAction
from action.LevelAction import LevelAction
from action.PauseAction import PauseAction
from pyoctoprintapifacade import OctoPrintApi


class FactoryAction:

    @staticmethod
    def get_cancel_action(printer_api: OctoPrintApi) -> AbstractAction:
        return CancelAction(printer_api)

    @staticmethod
    def get_cold_action(printer_api: OctoPrintApi) -> AbstractAction:
        return ColdAction(printer_api)

    @staticmethod
    def get_connect_action(printer_api: OctoPrintApi) -> AbstractAction:
        return ConnectAction(printer_api)

    @staticmethod
    def get_disconnect_action(printer_api: OctoPrintApi) -> AbstractAction:
        return DisconnectAction(printer_api)

    @staticmethod
    def get_home_action(printer_api: OctoPrintApi) -> AbstractAction:
        return HomeAction(printer_api)

    @staticmethod
    def get_hot_bed_action(printer_api: OctoPrintApi) -> AbstractAction:
        return HotBedAction(printer_api)

    @staticmethod
    def get_hot_tool_action(printer_api: OctoPrintApi) -> AbstractAction:
        return HotToolAction(printer_api)

    @staticmethod
    def get_level_action(printer_api: OctoPrintApi) -> AbstractAction:
        return LevelAction(printer_api)

    @staticmethod
    def get_pause_action(printer_api: OctoPrintApi) -> AbstractAction:
        return PauseAction(printer_api)
