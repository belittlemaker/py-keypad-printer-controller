from abc import abstractmethod
from action.AbstractAction import AbstractAction
from pyoctoprintapifacade import OctoPrintApi


class AbstractStatesAction(AbstractAction):

    __state: str

    def __init__(self, printer_api: OctoPrintApi):
        super().__init__(printer_api)
        self.reset()

    def reset(self) -> None:
        self._set_state(self._get_state_begin())

    def run(self, data: str = None) -> None:
        self._run_states().get(self.__state)(data)

    def _set_state(self, state: str) -> None:
        self.__state = state

    @abstractmethod
    def _get_state_begin(self) -> str:
        pass

    @abstractmethod
    def _run_states(self) -> dict:
        pass
