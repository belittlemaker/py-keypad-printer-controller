from abc import ABCMeta, abstractmethod
from config import GeneralConfig
from pyoctoprintapifacade import OctoPrintApi

from time import sleep


class AbstractAction(metaclass=ABCMeta):

    def __init__(self, printer_api: OctoPrintApi):
        self._printer: OctoPrintApi = printer_api

    def reset(self) -> None:
        pass

    def serial_match(self, serial_input: str) -> bool:
        return serial_input == self._get_serial()

    def sleep(self) -> None:
        sleep(self._get_time_sleep())

    @abstractmethod
    def run(self, data: str = None) -> None:
        pass

    @abstractmethod
    def _get_serial(self) -> str:
        pass

    def _get_time_sleep(self) -> int:
        return GeneralConfig.action_sleep
