from action.AbstractAction import AbstractAction


class ColdAction(AbstractAction):

    def run(self, data: str = None) -> None:
        self._printer.get_printer_facade().bed_target(0)
        self._printer.get_printer_facade().tool_target({'tool0': 0})

    def _get_serial(self) -> str:
        return "cold"
