from action.AbstractAction import AbstractAction
from config import OctoprintConfig


class ConnectAction(AbstractAction):

    def run(self, data: str = None) -> None:
        self._printer.get_connection_facade().connect(port=OctoprintConfig.port)

    def _get_serial(self) -> str:
        return "connect"
