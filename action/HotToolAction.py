from action.AbstractStatesAction import AbstractStatesAction
from pyoctoprintapifacade import OctoPrintApi


class HotToolAction(AbstractStatesAction):

    __STATE_HEATING = "heating"
    __STATE_INCREMENT = "increment"
    __TARGET_AUTOINCREMENT = 5

    def __init__(self, printer_api: OctoPrintApi):
        super().__init__(printer_api)

        self._target: int = 0

    def _get_serial(self) -> str:
        return "hot_tool"

    def _get_state_begin(self) -> str:
        return self.__STATE_HEATING

    def _run_states(self) -> dict:
        return {
            self.__STATE_HEATING: self.run_heating,
            self.__STATE_INCREMENT: self.run_increment
        }

    def _printer_target(self) -> None:
        self._printer.get_printer_facade().tool_target({'tool0': self._target})

    def run_heating(self, data: str = None) -> None:
        self._target = int(data)
        self._printer_target()
        self._set_state(self.__STATE_INCREMENT)

    def run_increment(self, data: str = None) -> None:
        self._target += self.__TARGET_AUTOINCREMENT
        self._printer_target()
