from action.AbstractStatesAction import AbstractStatesAction


class PauseAction(AbstractStatesAction):

    __STATE_PAUSE = 'pause'
    __STATE_UP = 'up'
    __STATE_DOWN = 'down'
    __STATE_RESUME = 'resume'

    def _run_states(self) -> dict:
        return {
            self.__STATE_PAUSE: self.run_pause,
            self.__STATE_UP: self.run_up,
            self.__STATE_DOWN: self.run_down,
            self.__STATE_RESUME: self.run_resume
        }

    def run_pause(self, data: str = None) -> None:
        self._printer.get_job_facade().pause()
        self._set_state(self.__STATE_UP)

    def run_up(self, data: str = None) -> None:
        self._printer.get_printer_facade().jog(z=10)
        self._set_state(self.__STATE_DOWN)

    def run_down(self, data: str = None) -> None:
        self._printer.get_printer_facade().jog(z=-10)
        self._set_state(self.__STATE_RESUME)

    def run_resume(self, data: str = None) -> None:
        self._printer.get_job_facade().resume()
        self._set_state(self.__STATE_PAUSE)

    def _get_serial(self) -> str:
        return "pause"

    def _get_state_begin(self) -> str:
        return self.__STATE_PAUSE
