from action.AbstractAction import AbstractAction


class CancelAction(AbstractAction):

    def run(self, data: str = None) -> None:
        self._printer.get_job_facade().cancel()

    def _get_serial(self) -> str:
        return "cancel"
