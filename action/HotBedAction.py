from action.HotToolAction import HotToolAction


class HotBedAction(HotToolAction):

    def _get_serial(self) -> str:
        return "hot_bed"

    def _printer_target(self) -> None:
        self._printer.get_printer_facade().bed_target(self._target)
