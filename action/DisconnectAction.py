from action.ConnectAction import ConnectAction


class DisconnectAction(ConnectAction):

    def run(self, data: str = None) -> None:
        self._printer.get_connection_facade().disconnect()
