import serial
from serial import Serial
from time import sleep
import logging


class SerialPort:

    __serial: Serial

    def __init__(self, path: str, baudrate: int, encoding: str):
        self.__baudrate = baudrate
        self.__encoding = encoding
        self.__path = path

    def close(self):
        self.__serial.close()

    def connect(self) -> None:
        while True:
            try:
                self.__serial = Serial(self.__path, baudrate=self.__baudrate)
                logging.debug("Serial Port (" + self.__path + ") connected.")
                break
            except (Exception, ValueError):
                sleep(10)

    def read(self) -> str:
        try:
            return self.__serial.readline().decode(self.__encoding)
        except serial.SerialException as exception:
            self.__reconnect(exception)
            return self.read()

    def write(self, output: str) -> None:
        try:
            self.__serial.write(bytes(output, encoding=self.__encoding))
        except serial.SerialException as exception:
            self.__reconnect(exception)
            self.write(output)

    def __reconnect(self, exception: serial.SerialException) -> None:
        logging.error(str(exception))
        self.connect()
