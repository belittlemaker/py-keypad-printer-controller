class Parser:

    def __init__(self):
        self.__data = ''
        self.__label = ''

    def parse(self, entry: str) -> None:
        self.__reset()
        if entry:
            entry_list: list = entry.strip().split()

            if len(entry_list) > 1:
                self.__label = entry_list[1]
                if len(entry_list) > 2:
                    self.__data = entry_list[2]

    def get_data(self) -> str:
        return self.__data

    def get_label(self) -> str:
        return self.__label

    def __reset(self):
        self.__data = ''
        self.__label = ''
